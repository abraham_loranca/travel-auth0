#Travel
 
version: v1.0
 
CRM – Customer Relationship Management.
 
 
### SET UP
* Requirements (Already covered with Docker deployment)
	1. Apache/2.4.27 or greater.
	2. MySQL 5.7 or greater.
	3. PHP/7.2.24 or greater.
 
  
* App Configuration
    1. Add host `travel.localhost`,
        	see [Edit hosts](https://dinahosting.com/ayuda/como-modificar-el-fichero-hosts).        	
    2. Create `.env` file from `example.env` and set it.
	4. Give Folder permissions:	
	    ```
		en linux
	    sudo chown -R $USER:www-data storage;
        chmod -R 775 storage;
        sudo chown -R $USER:www-data bootstrap/cache;
        chmod -R 775 bootstrap/cache;
	    ```
		
	7. Import database from `database/updates/*.sql` into `travel` DB
        with `root` user, at `localhost` host, `33063` port.
    8. Set `APP_KEY=base64:tt2qbEfp/qMWq6qF3gGvlfOzm+eLE+SptUXUv1lidvs=` at `.env`.     	
	9. Run `composer install`.
	10. Run `php artisan storage:link`. 
	
* Control de sesiones mediante auth0 
   1. documentacion  https://auth0.com/docs/logout
### CONTRIBUTION: Guidelines & Documentation
 
* Database Key Fields, tables and or values:
 
	1. `users.email`: Users email.
 
* Git :
    [Gitflow](http://nvie.com/posts/a-successful-git-branching-model).
* Back End:
    [Laravel 7.x](https://laravel.com/docs/7.x),
    [Laravel Voyager](https://docs.laravelvoyager.com).
* Front End:
    [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction),
 
***
 
2020 [Abraham Loranca](abrahamloranca@gmail.com)